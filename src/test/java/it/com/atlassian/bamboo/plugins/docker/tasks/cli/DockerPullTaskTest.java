package it.com.atlassian.bamboo.plugins.docker.tasks.cli;

import com.atlassian.bamboo.pageobjects.pages.plan.configuration.JobTaskConfigurationPage;
import com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator;
import com.atlassian.bamboo.testutils.model.TestBuildDetails;
import com.atlassian.bamboo.testutils.model.TestJobDetails;
import com.google.common.collect.ImmutableMap;
import it.com.atlassian.bamboo.plugins.docker.pageobjects.cli.DockerPullTaskComponent;
import it.com.atlassian.bamboo.plugins.docker.pageobjects.cli.DockerPushTaskComponent;
import it.com.atlassian.bamboo.plugins.docker.pageobjects.cli.DockerRunTaskComponent;
import it.com.atlassian.bamboo.plugins.docker.tasks.AbstractDockerTaskTest;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;

import java.util.Map;

public class DockerPullTaskTest extends AbstractDockerTaskTest
{
    @Test
    public void testTask() throws Exception
    {
        final TestBuildDetails plan = createAndSetupPlan();
        final TestJobDetails defaultJob = plan.getDefaultJob();
        final JobTaskConfigurationPage taskConfigurationPage = product.visit(JobTaskConfigurationPage.class, defaultJob);

        // add Docker run task to start the Docker registry container
        setupRunRegistryTask(taskConfigurationPage);

        // add Docker push task
        final Map<String, String> dockerPushTaskConfig = ImmutableMap.<String, String>builder()
                .put(DockerPushTaskComponent.REGISTRY_OPTION, DockerCliTaskConfigurator.REGISTRY_OPTION_CUSTOM)
                .put(DockerPushTaskComponent.REPOSITORY, "localhost:5000/scratch")
                .build();
        taskConfigurationPage.addNewTask(DockerPushTaskComponent.TASK_NAME, DockerPushTaskComponent.class, "Push repository to Docker registry", dockerPushTaskConfig);

        // add Docker pull task
        final Map<String, String> dockerPullTaskConfig = ImmutableMap.<String, String>builder()
                .put(DockerPullTaskComponent.REGISTRY_OPTION, DockerCliTaskConfigurator.REGISTRY_OPTION_CUSTOM)
                .put(DockerPullTaskComponent.REPOSITORY, "localhost:5000/scratch")
                .build();
        taskConfigurationPage.addNewTask(DockerPullTaskComponent.TASK_NAME, DockerPullTaskComponent.class, "Pull repository from Docker registry", dockerPullTaskConfig);

        product.gotoHomePage();
        backdoor.plans().triggerBuildAndAwaitSuccess(plan.getKey());
    }

    private void setupRunRegistryTask(@NotNull final JobTaskConfigurationPage taskConfigurationPage) throws Exception
    {
        final Map<String, String> dockerRunTaskConfig = ImmutableMap.<String, String>builder()
                .put(DockerRunTaskComponent.IMAGE, "registry")
                .put(DockerRunTaskComponent.DETACH, "true")
                .put(DockerRunTaskComponent.NAME, "registry")
                .put(DockerRunTaskComponent.CONTAINER_PORT_PREFIX + 0, "5000")
                .put(DockerRunTaskComponent.HOST_PORT_PREFIX + 0, "5000")
                .put(DockerRunTaskComponent.SERVICE_WAIT, "true")
                // Without this env variable the registry container will occasionally end unexpectedly and cause the test to fail -
                // "To avoid several Gunicorn workers racing to create the database, you should launch your registry with --preload."
                // (https://github.com/docker/docker-registry#sqlalchemy)
                .put(DockerRunTaskComponent.ENV_VARS, "GUNICORN_OPTS=[--preload]")
                .build();
        taskConfigurationPage.addNewTask(DockerRunTaskComponent.TASK_NAME, DockerRunTaskComponent.class, "Start Docker registry container", dockerRunTaskConfig);
    }
}