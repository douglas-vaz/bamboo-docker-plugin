package com.atlassian.bamboo.plugins.docker.client;

import com.atlassian.bamboo.plugins.docker.process.DockerProcessService;
import com.atlassian.bamboo.plugins.docker.process.ProcessCommand;
import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRule;

import java.io.File;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(JUnitParamsRunner.class)
public class DockerCmdTest
{
    private static final String TEST_DOCKER_PATH = "/usr/bin/docker";

    @Mock private DockerProcessService processService;
    @Rule
    public MockitoJUnitRule mockitoJUnitRule = new MockitoJUnitRule(this);

    private Docker docker;

    @Before
    public void setUp() throws Exception
    {
        this.docker = new DockerCmd(TEST_DOCKER_PATH, processService);
    }

    @Test
    public void testRun() throws Exception
    {
        final String imageName = "myImageName";

        docker.run(imageName, RunConfig.builder().build());
        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "run", "--rm", imageName).build());
    }

    @Test
    public void testRunCommand() throws Exception
    {
        final String imageName = "myImageName";

        docker.run(imageName, RunConfig.builder().command("echo \"hello world\"").build());
        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "run", "--rm", imageName, "echo", "\"hello world\"").build());
    }

    @Test
    public void testRunDetached() throws Exception
    {
        final String imageName = "myImageName";

        docker.run(imageName, RunConfig.builder().detach(true).build());
        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "run", "--detach", imageName).build());
    }

    @Test
    public void testRunContainerName() throws Exception
    {
        final String imageName = "myImageName";
        final String containerName = "myContainerName";

        docker.run(imageName, RunConfig.builder().containerName(containerName).build());
        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "run", "--rm", "--name", containerName, imageName).build());
    }

    @Test
    public void testRunPorts() throws Exception
    {
        final String imageName = "myImageName";
        final List<PortMapping> ports = ImmutableList.of(new PortMapping(8080, 8888), new PortMapping(5000, 5000));

        docker.run(imageName, RunConfig.builder().ports(ports).build());

        final ProcessCommand.Builder expectedCommandBuilder = ProcessCommand.builder()
                .add(TEST_DOCKER_PATH, "run", "--rm");
        for (PortMapping port : ports)
        {
            expectedCommandBuilder.add("-p", Joiner.on(':').join(port.getHostPort(), + port.getContainerPort()));
        }
        expectedCommandBuilder.add(imageName);

        verify(processService).execute(expectedCommandBuilder.build());
    }

    @Test
    public void testRunLinks() throws Exception
    {
        final String imageName = "myImageName";
        final Map<String, String> links = ImmutableMap.of("db", "db", "webapp", "webappA");

        docker.run(imageName, RunConfig.builder().links(links).build());

        final ProcessCommand.Builder expectedCommandListBuilder = ProcessCommand.builder()
                .add(TEST_DOCKER_PATH, "run");
        for (Map.Entry<String, String> link : links.entrySet())
        {
            expectedCommandListBuilder.add("--link", link.getKey() + ":" + link.getValue());
        }
        expectedCommandListBuilder.add("--rm", imageName);

        verify(processService).execute(expectedCommandListBuilder.build());
    }

    @Test
    public void testRunEnv() throws Exception
    {
        final String imageName = "myImageName";
        final Map<String, String> env = ImmutableMap.of("GUNICORN_OPTS", "[--preload]", "JAVA_OPTS", "-Xmx256m -Xms128m");

        docker.run(imageName, RunConfig.builder().env(env).build());

        final ProcessCommand.Builder expectedCommandListBuilder = ProcessCommand.builder()
                .add(TEST_DOCKER_PATH, "run", "--rm");
        for (Map.Entry<String, String> e : env.entrySet())
        {
            expectedCommandListBuilder.add("-e", e.getKey() + "=" + e.getValue());
        }
        expectedCommandListBuilder.add(imageName);

        verify(processService).execute(expectedCommandListBuilder.build());
    }

    @Test
    public void testRunAdditionalArgs() throws Exception
    {
        final String imageName = "myImageName";

        docker.run(imageName, RunConfig.builder().additionalArgs("--memory=\"64m\" -c 4").build());

        final ProcessCommand.Builder expectedCommandListBuilder = ProcessCommand.builder()
                .add(TEST_DOCKER_PATH, "run", "--rm", "--memory=\"64m\"", "-c", "4", imageName);

        verify(processService).execute(expectedCommandListBuilder.build());
    }

    @Test
    public void testRunVolumes() throws Exception
    {
        final String imageName = "myImageName";
        final List<DataVolume> volumes = ImmutableList.of(new DataVolume("/data", "/data"), new DataVolume("/test2", "/test1"));

        docker.run(imageName, RunConfig.builder().volumes(volumes).build());
        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "run", "--volume", "/data:/data",
                                                                    "--volume", "/test1:/test2", "--rm", imageName).build());
    }

    @Test
    public void testRunWorkDir() throws Exception
    {
        final String imageName = "myImageName";
        final String workdir = "/data";

        docker.run(imageName, RunConfig.builder().workDir(workdir).build());
        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "run", "--workdir", workdir, "--rm", imageName).build());
    }

    @Test
    @Parameters({"true, true", "false, false", "error, false"})
    public void testIsRunning(String output, boolean expectedResult) throws Exception
    {
        final String containerName = "myContainerName";

        when(processService.execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "inspect", "--format={{.State.Running}}", containerName).build())).thenReturn(output);

        assertThat(docker.isRunning(containerName), is(expectedResult));
    }

    @Test
    public void testGetHostPort() throws Exception
    {
        final String containerName = "myContainerName";
        final Integer hostPort = 47952;
        final Integer containerPort = 5000;

        when(processService.execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "inspect",
                                                                 "--format='{{(index (index .NetworkSettings.Ports \"5000/tcp\") 0).HostPort}}'",
                                                                 containerName).build())).thenReturn(String.valueOf(hostPort));
        assertThat(docker.getHostPort(containerName, containerPort), is(hostPort));

        when(processService.execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "inspect",
                                                                 "--format='{{(index (index .NetworkSettings.Ports \"5000/tcp\") 0).HostPort}}'",
                                                                 containerName).build())).thenReturn("error");
        assertThat(docker.getHostPort(containerName, containerPort), is(nullValue()));
    }

    @Test
    public void testRemove() throws Exception
    {
        final String containerName = "myContainerName";

        docker.remove(containerName);

        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "rm", "-f", containerName).build());
    }

    @Test
    public void testBuild() throws Exception
    {
        final File dockerFolder = new File("test");
        final String repository = "namespace/repository:tag";

        docker.build(dockerFolder, repository, BuildConfig.builder().build());

        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "build", "--force-rm=true", "--tag=\"" + repository + "\"", dockerFolder.getAbsolutePath()).build());
    }

    @Test
    public void testBuildNoCache() throws Exception
    {
        final File dockerFolder = new File("test");
        final String repository = "namespace/repository:tag";

        docker.build(dockerFolder, repository, BuildConfig.builder().nocache(true).build());

        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "build", "--no-cache=true", "--force-rm=true", "--tag=\"" + repository + "\"", dockerFolder.getAbsolutePath()).build());
    }

    @Test
    public void testSave() throws Exception
    {
        final String filename = "myRepository.tar";
        final String repository = "namespace/repository:tag";

        docker.save(filename, repository);

        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "save", "--output=\"" + filename + "\"", repository).build());
    }

    @Test
    public void testTag() throws Exception
    {
        final String image = "namespace/repository:latest";
        final String repository = "namespace/repository";
        final String tag = "v15";

        docker.tag(image, repository, tag);

        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "tag", image, repository + ':' + tag).build());
    }

    @Test
    public void testPush() throws Exception
    {
        final String repository = "namespace/repository";

        docker.push(repository, AuthConfig.builder().build());

        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "push", repository).build());
    }

    @Test
    public void testPushAuth() throws Exception
    {
        final String repository = "namespace/repository";
        final String username = "roland";
        final String password = "12345";
        final String email = "roland@druidia.com";

        docker.push(repository, AuthConfig.builder()
                .username(username)
                .password(password)
                .email(email)
                .build());

        verify(processService).executeSilently(ProcessCommand.builder().add(TEST_DOCKER_PATH, "login", "-u", username, "-p", password, "-e", email).mask(password).build());
        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "push", repository).build());
        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "logout").build());
    }

    @Test
    public void testPushAuthRegistry() throws Exception
    {
        final String repository = "namespace/repository";
        final String registry = "registry.druidia.com:12345";
        final String username = "roland";
        final String password = "12345";
        final String email = "roland@druidia.com";

        docker.push(repository, AuthConfig.builder()
                .registryAddress(registry)
                .username(username)
                .password(password)
                .email(email)
                .build());

        verify(processService).executeSilently(ProcessCommand.builder().add(TEST_DOCKER_PATH, "login", "-u", username, "-p", password, "-e", email, registry).mask(password).build());
        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "push", repository).build());
        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "logout", registry).build());
    }

    @Test
    public void testPushAuthPreservesDockercfg() throws Exception
    {
        final String repository = "namespace/repository";
        final String username = "roland";
        final String password = "12345";
        final String email = "roland@druidia.com";
        final String dockercfg = "ludicrous";

        final File USER_HOME = new File(System.getProperty("user.home"));
        final File DOCKERCFG = new File(USER_HOME, ".dockercfg");
        FileUtils.write(DOCKERCFG, dockercfg);

        final ProcessCommand loginCommand = ProcessCommand.builder().add(TEST_DOCKER_PATH, "login", "-u", username, "-p", password, "-e", email).mask(password).build();
        when(processService.executeSilently(loginCommand)).then(invocation -> {
            FileUtils.write(DOCKERCFG, "speed");
            return null;
        });

        docker.push(repository, AuthConfig.builder()
                .username(username)
                .password(password)
                .email(email)
                .build());

        verify(processService).executeSilently(loginCommand);
        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "push", repository).build());
        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "logout").build());

        assertThat(FileUtils.readFileToString(DOCKERCFG), equalTo(dockercfg));
    }

    @Test
    public void testPull() throws Exception
    {
        final String repository = "namespace/repository";

        docker.pull(repository, AuthConfig.builder().build());

        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "pull", repository).build());
    }

    @Test
    public void testPullAuth() throws Exception
    {
        final String repository = "namespace/repository";
        final String username = "roland";
        final String password = "12345";
        final String email = "roland@druidia.com";

        docker.pull(repository, AuthConfig.builder()
                .username(username)
                .password(password)
                .email(email)
                .build());

        verify(processService).executeSilently(ProcessCommand.builder().add(TEST_DOCKER_PATH, "login", "-u", username, "-p", password, "-e", email).mask(password).build());
        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "pull", repository).build());
        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "logout").build());
    }

    @Test
    public void testPullAuthRegistry() throws Exception
    {
        final String repository = "namespace/repository";
        final String registry = "registry.druidia.com:12345";
        final String username = "roland";
        final String password = "12345";
        final String email = "roland@druidia.com";

        docker.pull(repository, AuthConfig.builder()
                .registryAddress(registry)
                .username(username)
                .password(password)
                .email(email)
                .build());

        verify(processService).executeSilently(ProcessCommand.builder().add(TEST_DOCKER_PATH, "login", "-u", username, "-p", password, "-e", email, registry).mask(password).build());
        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "pull", repository).build());
        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "logout", registry).build());
    }

    @Test
    public void testPullAuthPreservesDockercfg() throws Exception
    {
        final String repository = "namespace/repository";
        final String username = "roland";
        final String password = "12345";
        final String email = "roland@druidia.com";
        final String dockercfg = "ludicrous";

        final File USER_HOME = new File(System.getProperty("user.home"));
        final File DOCKERCFG = new File(USER_HOME, ".dockercfg");
        FileUtils.write(DOCKERCFG, dockercfg);

        final ProcessCommand loginCommand = ProcessCommand.builder().add(TEST_DOCKER_PATH, "login", "-u", username, "-p", password, "-e", email).mask(password).build();
        when(processService.executeSilently(loginCommand)).then(invocation -> {
            FileUtils.write(DOCKERCFG, "speed");
            return null;
        });

        docker.pull(repository, AuthConfig.builder()
                .username(username)
                .password(password)
                .email(email)
                .build());

        verify(processService).executeSilently(loginCommand);
        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "pull", repository).build());
        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "logout").build());

        assertThat(FileUtils.readFileToString(DOCKERCFG), equalTo(dockercfg));
    }
}