package com.atlassian.bamboo.plugins.docker.client;

import com.atlassian.bamboo.plugins.docker.process.DockerProcessService;
import com.atlassian.bamboo.plugins.docker.process.ProcessCommand;
import com.atlassian.bamboo.process.CommandlineStringUtils;
import com.atlassian.utils.process.ProcessException;
import com.google.common.base.Joiner;
import com.google.common.primitives.Ints;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.util.Map;

public class DockerCmd implements Docker
{
    // ------------------------------------------------------------------------------------------------------- Constants
    private static final Logger log = Logger.getLogger(DockerCmd.class);

    private static final File USER_HOME = new File(System.getProperty("user.home"));
    private static final File DOCKERCFG = new File(USER_HOME, ".dockercfg");
    private static final File DOCKERCFG_BACKUP = new File(USER_HOME, ".dockercfg.bamboo");

    // ------------------------------------------------------------------------------------------------- Type Properties
    private final String dockerPath;
    private final DockerProcessService processService;

    // ---------------------------------------------------------------------------------------------------- Dependencies
    // ---------------------------------------------------------------------------------------------------- Constructors
    public DockerCmd(@NotNull final String dockerPath, @NotNull final DockerProcessService processService)
    {
        this.dockerPath = dockerPath;
        this.processService = processService;
    }

    // ----------------------------------------------------------------------------------------------- Interface Methods
    @Override
    public void run(@NotNull final String imageName, @NotNull final RunConfig runConfig) throws DockerException
    {
        final ProcessCommand.Builder commandBuilder = ProcessCommand.builder()
                .add(dockerPath, "run");

        for (DataVolume volumes : runConfig.getVolumes())
        {
            commandBuilder.add("--volume", Joiner.on(':').skipNulls().join(volumes.getHostDirectory(), volumes.getContainerDataVolume()));
        }

        if (runConfig.getWorkDir().isDefined())
        {
            commandBuilder.add("--workdir", runConfig.getWorkDir().get());
        }

        for (Map.Entry<String, String> links : runConfig.getLinks().entrySet())
        {
            commandBuilder.add("--link", Joiner.on(':').join(links.getKey(), links.getValue()));
        }

        if (runConfig.isDetach())
        {
            commandBuilder.add("--detach");
        }
        else
        {
            commandBuilder.add("--rm");
        }

        if (runConfig.getContainerName().isDefined())
        {
            commandBuilder.add("--name", runConfig.getContainerName().get());
        }

        for (PortMapping ports : runConfig.getPorts())
        {
            commandBuilder.add("-p", Joiner.on(':').skipNulls().join(ports.getHostPort(), ports.getContainerPort()));
        }

        for (Map.Entry<String, String> env : runConfig.getEnv().entrySet())
        {
            commandBuilder.add("-e", String.format(EQ_ARG, env.getKey(), env.getValue()));
        }

        if (runConfig.getAdditionalArgs().isDefined())
        {
            commandBuilder.addAll(CommandlineStringUtils.tokeniseCommandline(runConfig.getAdditionalArgs().get()));
        }

        commandBuilder.add(imageName);

        if (runConfig.getCommand().isDefined())
        {
            commandBuilder.addAll(CommandlineStringUtils.tokeniseCommandline(runConfig.getCommand().get()));
        }

        try
        {
            processService.execute(commandBuilder.build());
        }
        catch (ProcessException e)
        {
            throw new DockerException("Error running Docker run command", e);
        }
    }

    @Override
    public boolean isRunning(@NotNull final String containerName) throws DockerException
    {
        final ProcessCommand inspectCommand = ProcessCommand.builder()
                .add(dockerPath, "inspect", "--format={{.State.Running}}", containerName)
                .build();
        final String output;

        try
        {
            output = processService.execute(inspectCommand);
        }
        catch (ProcessException e)
        {
            throw new DockerException("Error running Docker inspect command", e);
        }

        return Boolean.parseBoolean(output);
    }

    @Nullable
    @Override
    public Integer getHostPort(@NotNull final String containerName, @NotNull final Integer containerPort) throws DockerException
    {
        final ProcessCommand inspectCommand = ProcessCommand.builder()
                .add(dockerPath, "inspect")
                .add(String.format("--format='{{(index (index .NetworkSettings.Ports \"%d/tcp\") 0).HostPort}}'", containerPort))
                .add(containerName)
                .build();
        final String output;

        try
        {
            output = processService.execute(inspectCommand);
        }
        catch (ProcessException e)
        {
            throw new DockerException("Error running Docker inspect command", e);
        }

        return Ints.tryParse(output);
    }

    @Override
    public void remove(@NotNull final String containerName) throws DockerException
    {
        final ProcessCommand removeCommand = ProcessCommand.builder()
                .add(dockerPath, "rm", "-f", containerName)
                .build();

        try
        {
            processService.execute(removeCommand);
        }
        catch (ProcessException e)
        {
            throw new DockerException("Error running Docker remove command", e);
        }
    }

    @Override
    public void build(@NotNull final File dockerFolder, @NotNull final String repository,
                      @NotNull final BuildConfig buildConfig) throws DockerException
    {
        final ProcessCommand.Builder buildCommand = ProcessCommand.builder()
                .add(dockerPath, "build");

        if (buildConfig.isNoCache())
        {
            buildCommand.add("--no-cache=true");
        }

        buildCommand.add("--force-rm=true");

        buildCommand.add("--tag=\"" + repository + "\"", dockerFolder.getAbsolutePath());

        try
        {
            processService.execute(buildCommand.build());
        }
        catch (ProcessException e)
        {
            throw new DockerException("Error running Docker build command", e);
        }
    }

    @Override
    public void save(@NotNull final String filename, @NotNull final String repository) throws DockerException
    {
        final ProcessCommand saveCommand = ProcessCommand.builder()
                .add(dockerPath, "save", "--output=\"" + filename + "\"", repository)
                .build();

        try
        {
            processService.execute(saveCommand);
        }
        catch (ProcessException e)
        {
            throw new DockerException("Error running Docker save command", e);
        }
    }

    @Override
    public void tag(@NotNull final String image, @NotNull final String repository, @NotNull final String tag) throws DockerException
    {
        final ProcessCommand tagCommand = ProcessCommand.builder()
                .add(dockerPath, "tag", image, Joiner.on(':').join(repository, tag))
                .build();

        try
        {
            processService.execute(tagCommand);
        }
        catch (ProcessException e)
        {
            throw new DockerException("Error running Docker tag command", e);
        }
    }

    @Override
    public void push(@NotNull final String repository, @NotNull final AuthConfig authConfig) throws DockerException
    {
        final Command pushCommand = () -> processService.execute(ProcessCommand.builder()
                                       .add(dockerPath, "push", repository)
                                       .build());
        final Command command = authConfig.isAuthorisationProvided()
                                ? new AuthorisedCommand(authConfig, pushCommand) : pushCommand;

        try
        {
            command.execute();
        }
        catch (ProcessException e)
        {
            final StringBuilder message = new StringBuilder();
            // The error reason (if one is given) will always occur above this message in the log,
            // i.e. the process stderr is logged before the ProcessException is thrown.
            message.append("Error running Docker push command. See messages above for details.");

            if (!authConfig.isAuthorisationProvided())
            {
                message.append(" If no reason is specified it may be because you have not provided")
                       .append(" login details to a registry that requires authorisation.");
            }

            throw new DockerException(message.toString(), e);
        }
    }

    @Override
    public void pull(@NotNull final String repository, @NotNull final AuthConfig authConfig) throws DockerException {
        final Command pullCommand = () -> processService.execute(ProcessCommand.builder()
                                                                         .add(dockerPath, "pull", repository)
                                                                         .build());
        final Command command = authConfig.isAuthorisationProvided()
                                ? new AuthorisedCommand(authConfig, pullCommand) : pullCommand;

        try
        {
            command.execute();
        }
        catch (ProcessException e)
        {
            final StringBuilder message = new StringBuilder();
            // The error reason (if one is given) will always occur above this message in the log,
            // i.e. the process stderr is logged before the ProcessException is thrown.
            message.append("Error running Docker pull command. See messages above for details.");

            if (!authConfig.isAuthorisationProvided())
            {
                message.append(" If no reason is specified it may be because you have not provided")
                       .append(" login details to a registry that requires authorisation.");
            }

            throw new DockerException(message.toString(), e);
        }
    }

    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Public Methods
    // -------------------------------------------------------------------------------------------------- Helper Methods
    private interface Command
    {
        void execute() throws ProcessException;
    }

    private class AuthorisedCommand implements Command
    {
        private final AuthConfig authConfig;
        private final Command command;

        private AuthorisedCommand(@NotNull final AuthConfig authConfig, @NotNull final Command command)
        {
            this.authConfig = authConfig;
            this.command = command;
        }

        @Override
        public void execute() throws ProcessException
        {
            try
            {
                backupDockercfg();
                login();
                command.execute();
            }
            finally
            {
                logout();
                restoreDockercfg();
            }
        }

        private void backupDockercfg()
        {
            if (DOCKERCFG.exists())
            {
                try
                {
                    FileUtils.copyFile(DOCKERCFG, DOCKERCFG_BACKUP, true);
                }
                catch (IOException e)
                {
                    log.warn("Unable to backup '.dockercfg' file. After the build, make sure the agent's '~/.dockercfg' file is still valid. ", e);
                }
            }
        }

        private void login() throws ProcessException
        {
            final ProcessCommand.Builder loginCommand = ProcessCommand.builder()
                    .add(dockerPath, "login", "-u", authConfig.getUsername().get(), "-p", authConfig.getPassword().get(), "-e", authConfig.getEmail().get())
                    .mask(authConfig.getPassword().get());
            if (!authConfig.getRegistryAddress().isEmpty())
            {
                loginCommand.add(authConfig.getRegistryAddress().get());
            }
            // Execute silently so that we don't log the password
            processService.executeSilently(loginCommand.build());
        }

        private void logout() throws ProcessException
        {
            final ProcessCommand.Builder logoutCommand = ProcessCommand.builder().add(dockerPath, "logout");
            if (!authConfig.getRegistryAddress().isEmpty())
            {
                logoutCommand.add(authConfig.getRegistryAddress().get());
            }
            processService.execute(logoutCommand.build());
        }

        private void restoreDockercfg()
        {
            if (DOCKERCFG_BACKUP.exists())
            {
                try
                {
                    DOCKERCFG.delete();
                    FileUtils.moveFile(DOCKERCFG_BACKUP, DOCKERCFG);
                }
                catch (IOException e)
                {
                    log.warn("Unable to restore '.dockercfg' file. After the build, make sure the agent's '~/.dockercfg' file is still valid.", e);
                }
            }
        }
    }

    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators
}
