package com.atlassian.bamboo.plugins.docker.validation;

import com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator;
import com.atlassian.bamboo.task.TaskContextHelperService;
import com.atlassian.sal.api.message.I18nResolver;
import org.jetbrains.annotations.NotNull;

public class ConfigValidatorFactoryImpl implements ConfigValidatorFactory
{
    private final TaskContextHelperService taskContextHelper;
    private final I18nResolver i18nResolver;

    public ConfigValidatorFactoryImpl(TaskContextHelperService taskContextHelper, @NotNull final I18nResolver i18nResolver)
    {
        this.taskContextHelper = taskContextHelper;
        this.i18nResolver = i18nResolver;
    }

    @NotNull
    @Override
    public ConfigValidator create(@NotNull final String dockerCommandOption)
    {
        if (DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION_BUILD.equals(dockerCommandOption))
        {
            return new BuildConfigValidator(i18nResolver);
        }

        if (DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION_RUN.equals(dockerCommandOption))
        {
            return new RunConfigValidator(i18nResolver, taskContextHelper);
        }

        if (DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION_PUSH.equals(dockerCommandOption))
        {
            return new PushConfigValidator(i18nResolver);
        }

        if (DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION_PULL.equals(dockerCommandOption))
        {
            return new PullConfigValidator(i18nResolver);
        }

        throw new IllegalArgumentException("No validation service for option " + dockerCommandOption);
    }
}
