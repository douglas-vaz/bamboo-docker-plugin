package com.atlassian.bamboo.plugins.docker.tasks.cli;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.plugins.docker.PluginConstants;
import com.atlassian.bamboo.plugins.docker.validation.ConfigValidator;
import com.atlassian.bamboo.plugins.docker.validation.ConfigValidatorFactory;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.TaskContextHelperService;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.TaskPredicates;
import com.atlassian.bamboo.task.TaskRequirementSupport;
import com.atlassian.bamboo.utils.BambooPredicates;
import com.atlassian.bamboo.utils.Pair;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.bamboo.v2.build.agent.capability.Requirement;
import com.atlassian.bamboo.v2.build.agent.capability.RequirementImpl;
import com.atlassian.sal.api.message.I18nResolver;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.atlassian.bamboo.plugins.docker.DockerCapabilityTypeModule.DOCKER_CAPABILITY;
import static com.atlassian.bamboo.plugins.docker.tasks.DockerTaskPredicates.isConfigurationFieldEqual;
import static com.atlassian.bamboo.task.TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES;
import static com.atlassian.bamboo.task.TaskConfigConstants.CFG_WORKING_SUB_DIRECTORY;
import static com.atlassian.bamboo.task.TaskPredicates.isTaskDefinitionPluginKeyEqual;
import static com.atlassian.bamboo.task.TaskPredicates.isTaskEnabled;
import static com.atlassian.bamboo.utils.predicates.TextPredicates.startsWith;

public class DockerCliTaskConfigurator extends AbstractTaskConfigurator implements TaskRequirementSupport
{
    @SuppressWarnings("UnusedDeclaration")
    private static final Logger log = Logger.getLogger(DockerCliTaskConfigurator.class);
    // ------------------------------------------------------------------------------------------------------- Constants
    public static final String DOCKER_CLI_TASK_KEY = PluginConstants.DOCKER_PLUGIN_KEY + ":task.docker.cli";

    public static final String DOCKER_COMMAND_OPTIONS = "commandOptions";
    public static final String DOCKER_COMMAND_OPTION_BUILD = "build";
    public static final String DOCKER_COMMAND_OPTION_RUN = "run";
    public static final String DOCKER_COMMAND_OPTION_PUSH = "push";
    public static final String DOCKER_COMMAND_OPTION_PULL = "pull";

    public static final String DOCKER_COMMAND_OPTION = "commandOption";

    // Build
    public static final String DOCKERFILE_OPTIONS = "dockerfileOptions";
    public static final String DOCKERFILE_OPTION_EXISTING = "existing";
    public static final String DOCKERFILE_OPTION_INLINE = "inline";

    public static final String DOCKERFILE_OPTION = "dockerfileOption";
    public static final String DOCKERFILE = "dockerfile";
    public static final String REPOSITORY = "repository";
    public static final String NOCACHE = "nocache";
    public static final String SAVE = "save";
    public static final String FILENAME = "filename";

    public static final Set<String> BUILD_FIELD_KEYS = ImmutableSet.<String>builder()
            .add(DOCKERFILE_OPTION)
            .add(DOCKERFILE)
            .add(REPOSITORY)
            .add(NOCACHE)
            .add(SAVE)
            .add(FILENAME)
            .build();

    // Run
    public static final String DETACHED_CONTAINERS = "detachedContainers";

    public static final String IMAGE = "image";
    public static final String COMMAND = "command";
    public static final String NAME = "name";
    public static final String DETACH = "detach";
    public static final String LINK = "link";
    public static final String ENV_VARS = "envVars";
    public static final String ADDITIONAL_ARGS = "additionalArgs";

    public static final String WORK_DIR = "workDir";
    public static final String VOLUMES_INDICES = "volumesIndices";
    public static final String HOST_DIRECTORY_PREFIX = "hostDirectory_";
    public static final String CONTAINER_DATA_VOLUME_PREFIX = "containerDataVolume_";

    public static final String PORTS_INDICES = "portsIndices";
    public static final String CONTAINER_PORT_PREFIX = "containerPort_";
    public static final String HOST_PORT_PREFIX = "hostPort_";

    public static final String SERVICE_WAIT = "serviceWait";
    public static final String SERVICE_URL_PATTERN = "serviceUrlPattern";
    public static final String SERVICE_TIMEOUT = "serviceTimeout";

    private static final Set<String> RUN_FIELD_KEYS = ImmutableSet.<String>builder()
            .add(IMAGE)
            .add(COMMAND)
            .add(NAME)
            .add(DETACH)
            .add(LINK)
            .add(ENV_VARS)
            .add(WORK_DIR)
            .add(ADDITIONAL_ARGS)
            .add(SERVICE_WAIT)
            .add(SERVICE_URL_PATTERN)
            .add(SERVICE_TIMEOUT)
            .build();

    public static final String DEFAULT_CONTAINER_WORK_DIR = "/data";
    public final static String TASK_WORK_DIR_PLACEHOLDER = "${bamboo.working.directory}";

    public final static String SERVICE_URL_PORT_PLACEHOLDER = "${docker.port}";

    public static final String DEFAULT_CONTAINER_SERVICE_URL = "http://localhost:" + SERVICE_URL_PORT_PLACEHOLDER;
    public static final long DEFAULT_TIMEOUT_SECONDS = 120;

    public static final Predicate<TaskDefinition> isDockerRunDetachedTask = Predicates.and(
            isTaskDefinitionPluginKeyEqual(DOCKER_CLI_TASK_KEY),
            isConfigurationFieldEqual(DOCKER_COMMAND_OPTION, DOCKER_COMMAND_OPTION_RUN),
            isConfigurationFieldEqual(DockerCliTaskConfigurator.DETACH, true),
            isTaskEnabled());

    private final static Predicate<String> isVolumeField = Predicates.or(
            startsWith(CONTAINER_DATA_VOLUME_PREFIX), startsWith(HOST_DIRECTORY_PREFIX));

    private final static Predicate<String> isPortField = Predicates.or(
            startsWith(CONTAINER_PORT_PREFIX), startsWith(HOST_PORT_PREFIX));

    // Push
    public static final String REGISTRY_OPTIONS = "registryOptions";
    public static final String REGISTRY_OPTION_HUB = "hub";
    public static final String REGISTRY_OPTION_CUSTOM = "custom";

    public static final String PUSH_REPOSITORY = "pushRepository";
    public static final String REGISTRY_OPTION = "registryOption";
    public static final String USERNAME = "username";
    public static final String CHANGE_PASSWORD = "changePassword";
    public static final String PASSWORD = "password";
    public static final String EMAIL = "email";

    private static final Set<String> PUSH_FIELD_KEYS = ImmutableSet.<String>builder()
            .add(PUSH_REPOSITORY)
            .add(REGISTRY_OPTION)
            .add(USERNAME)
            .add(PASSWORD)
            .add(EMAIL)
            .build();

    // Pull
    public static final String PULL_REPOSITORY = "pullRepository";
    public static final String PULL_REGISTRY_OPTION = "pullRegistryOption";
    public static final String PULL_USERNAME = "pullUsername";
    public static final String PULL_CHANGE_PASSWORD = "pullChangePassword";
    public static final String PULL_PASSWORD = "pullPassword";
    public static final String PULL_EMAIL = "pullEmail";

    private static final Set<String> PULL_FIELD_KEYS = ImmutableSet.<String>builder()
            .add(PULL_REPOSITORY)
            .add(PULL_REGISTRY_OPTION)
            .add(PULL_USERNAME)
            .add(PULL_PASSWORD)
            .add(PULL_EMAIL)
            .build();

    private static final Set<String> FIELD_KEYS = ImmutableSet.<String>builder()
            .add(DOCKER_COMMAND_OPTION)
            .addAll(BUILD_FIELD_KEYS)
            .addAll(RUN_FIELD_KEYS)
            .addAll(PUSH_FIELD_KEYS)
            .addAll(PULL_FIELD_KEYS)
            .add(CFG_ENVIRONMENT_VARIABLES)
            .add(CFG_WORKING_SUB_DIRECTORY)
            .build();

    // ------------------------------------------------------------------------------------------------- Type Properties
    // ---------------------------------------------------------------------------------------------------- Dependencies
    private I18nResolver i18nResolver;
    private TaskContextHelperService taskContextHelper;
    private ConfigValidatorFactory validatorFactory;

    // ---------------------------------------------------------------------------------------------------- Constructors
    // ----------------------------------------------------------------------------------------------- Interface Methods
    @NotNull
    @Override
    public Set<Requirement> calculateRequirements(@NotNull TaskDefinition taskDefinition)
    {
        return ImmutableSet.<Requirement>of(new RequirementImpl(DOCKER_CAPABILITY, true, ".*"));
    }

    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull ActionParametersMap params, @Nullable TaskDefinition previousTaskDefinition)
    {
        final Map<String, String> map = super.generateTaskConfigMap(params, previousTaskDefinition);
        taskConfiguratorHelper.populateTaskConfigMapWithActionParameters(map, params, FIELD_KEYS);

        // Run
        for (String key : Iterables.filter(params.keySet(), isVolumeField))
        {
            map.put(key, params.getString(key));
        }

        for (String key : Iterables.filter(params.keySet(), isPortField))
        {
            map.put(key, params.getString(key));
        }

        // Push
        if (!params.getBoolean(CHANGE_PASSWORD) && previousTaskDefinition != null)
        {
            map.put(PASSWORD, previousTaskDefinition.getConfiguration().get(PASSWORD));
        }

        // Pull
        if (!params.getBoolean(PULL_CHANGE_PASSWORD) && previousTaskDefinition != null)
        {
            map.put(PULL_PASSWORD, previousTaskDefinition.getConfiguration().get(PULL_PASSWORD));
        }

        return map;
    }

    @Override
    public void populateContextForCreate(@NotNull Map<String, Object> context)
    {
        super.populateContextForCreate(context);

        context.put(DOCKER_COMMAND_OPTIONS, getDockerCommandOptions());
        context.put(DOCKER_COMMAND_OPTION, DOCKER_COMMAND_OPTION_BUILD);

        // Build
        context.put(DOCKERFILE_OPTIONS, getDockerfileOptions());
        context.put(DOCKERFILE_OPTION, DOCKERFILE_OPTION_INLINE);

        // Run
        context.put(WORK_DIR, DEFAULT_CONTAINER_WORK_DIR);
        context.put(VOLUMES_INDICES, ImmutableList.of(0));
        context.put(HOST_DIRECTORY_PREFIX + 0, TASK_WORK_DIR_PLACEHOLDER);
        context.put(CONTAINER_DATA_VOLUME_PREFIX + 0, DEFAULT_CONTAINER_WORK_DIR);

        context.put(PORTS_INDICES, Collections.EMPTY_LIST);
        context.put(SERVICE_URL_PATTERN, DEFAULT_CONTAINER_SERVICE_URL);
        context.put(SERVICE_TIMEOUT, DEFAULT_TIMEOUT_SECONDS);

        final List<TaskDefinition> tasks = taskContextHelper.getTasks(context);
        context.put(DETACHED_CONTAINERS, ImmutableList.copyOf(Iterables.transform(
                Iterables.filter(tasks, Predicates.and(isDockerRunDetachedTask, TaskPredicates.isFinalisingEqual(false))),
                new TaskDefinitionToNameFunction())));

        // Push
        context.put(REGISTRY_OPTIONS, getRegistryOptions());
        context.put(REGISTRY_OPTION, REGISTRY_OPTION_HUB);

        // Pull
        context.put(PULL_REGISTRY_OPTION, REGISTRY_OPTION_HUB);
    }

    @Override
    public void populateContextForEdit(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition)
    {
        super.populateContextForEdit(context, taskDefinition);
        context.putAll(taskDefinition.getConfiguration());

        context.put(DOCKER_COMMAND_OPTIONS, getDockerCommandOptions());

        // Build
        context.put(DOCKERFILE_OPTIONS, getDockerfileOptions());

        // Run
        final List<TaskDefinition> tasks = taskContextHelper.getTasksBeforeTaskId(context, taskDefinition.getId());
        context.put(DETACHED_CONTAINERS, ImmutableList.copyOf(Iterables.transform(
                Iterables.filter(tasks, isDockerRunDetachedTask),
                new TaskDefinitionToNameFunction())));

        context.put(VOLUMES_INDICES, generateVolumesList(taskDefinition));
        context.put(PORTS_INDICES, generatePortsList(taskDefinition));

        // Push / Pull
        context.put(REGISTRY_OPTIONS, getRegistryOptions());

        if (!context.containsKey(PULL_REGISTRY_OPTION)) {
            context.put(PULL_REGISTRY_OPTION, REGISTRY_OPTION_HUB);
        }
    }

    @Override
    public void validate(@NotNull final ActionParametersMap params, @NotNull final ErrorCollection errorCollection)
    {
        super.validate(params, errorCollection);

        final ConfigValidator validator = validatorFactory.create(params.getString(DOCKER_COMMAND_OPTION));
        validator.validate(params, errorCollection);
    }

    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Public Methods
    // -------------------------------------------------------------------------------------------------- Helper Methods
    @NotNull
    private List<Pair<String, String>> getDockerCommandOptions()
    {
        return ImmutableList.of(
            Pair.make(DOCKER_COMMAND_OPTION_BUILD, i18nResolver.getText("docker.command.build")),
            Pair.make(DOCKER_COMMAND_OPTION_RUN, i18nResolver.getText("docker.command.run")),
            Pair.make(DOCKER_COMMAND_OPTION_PUSH, i18nResolver.getText("docker.command.push")),
            Pair.make(DOCKER_COMMAND_OPTION_PULL, i18nResolver.getText("docker.command.pull")));
    }

    // Build
    @NotNull
    private List<Pair<String, String>> getDockerfileOptions()
    {
        return ImmutableList.of(
                Pair.make(DOCKERFILE_OPTION_EXISTING, i18nResolver.getText("docker.dockerfile.existing")),
                Pair.make(DOCKERFILE_OPTION_INLINE, i18nResolver.getText("docker.dockerfile.inline"))
        );
    }

    // Run
    private List<Integer> generateVolumesList(final TaskDefinition taskDefinition)
    {
        final List<Integer> volumesList = Lists.newArrayList();
        for (String key : Iterables.filter(taskDefinition.getConfiguration().keySet(), BambooPredicates.startsWith(CONTAINER_DATA_VOLUME_PREFIX)))
        {
            volumesList.add(Integer.valueOf(StringUtils.removeStart(key, CONTAINER_DATA_VOLUME_PREFIX)));
        }
        return volumesList;
    }

    private List<Integer> generatePortsList(final TaskDefinition taskDefinition)
    {
        final List<Integer> portsList = Lists.newArrayList();
        for (String key : Iterables.filter(taskDefinition.getConfiguration().keySet(), BambooPredicates.startsWith(CONTAINER_PORT_PREFIX)))
        {
            portsList.add(Integer.valueOf(StringUtils.removeStart(key, CONTAINER_PORT_PREFIX)));
        }
        return portsList;
    }

    private class TaskDefinitionToNameFunction implements Function<TaskDefinition, String>
    {
        @Override
        public String apply(@NotNull TaskDefinition taskDefinition)
        {
            return taskDefinition.getConfiguration().get(NAME);
        }
    }

    // Push / Pull
    @NotNull
    private List<Pair<String, String>> getRegistryOptions()
    {
        return ImmutableList.of(
                Pair.make(REGISTRY_OPTION_HUB, i18nResolver.getText("docker.registry.hub")),
                Pair.make(REGISTRY_OPTION_CUSTOM, i18nResolver.getText("docker.registry.custom"))
        );
    }

    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators
    public void setI18nResolver(I18nResolver i18nResolver)
    {
        this.i18nResolver = i18nResolver;
    }

    public void setTaskContextHelper(TaskContextHelperService taskContextHelper)
    {
        this.taskContextHelper = taskContextHelper;
    }

    public void setValidatorFactory(ConfigValidatorFactory validatorFactory)
    {
        this.validatorFactory = validatorFactory;
    }
}