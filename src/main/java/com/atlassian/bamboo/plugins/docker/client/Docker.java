package com.atlassian.bamboo.plugins.docker.client;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;

public interface Docker
{
    static final String EQ_ARG = "%s=%s";

    /**
     * Start a Docker container for the image provided.
     *
     * @param imageName the image to run.
     * @param runConfig The run configuration describing which parameters
     *                  to include for the Docker run command.
     */
    void run(@NotNull final String imageName, @NotNull final RunConfig runConfig) throws DockerException;

    /**
     * Checks if a container with the given name is running. If the
     * container exists but has exited, this method will return false.
     *
     * @param containerName the name of the container to check.
     * @return true if the container is running, false otherwise.
     */
    boolean isRunning(@NotNull final String containerName) throws DockerException;

    /**
     * Gets the host port mapped to the given container port.
     *
     * @param containerName the name of the container to check.
     * @param containerPort The container port.
     * @return The host port or null if there is no host port
     * mapped to the container port.
     */
    @Nullable
    Integer getHostPort(@NotNull final String containerName, @NotNull final Integer containerPort) throws DockerException;

    /**
     * Removes the container with the given name. This method will
     * forcibly remove the container if it is running.
     *
     * @param containerName the name of the container to remove.
     */
    void remove(@NotNull final String containerName) throws DockerException;

    /**
     * Builds a Docker image using the Dockerfile found in the given
     * folder location and tags the built image with the given repository
     * label.
     *
     * @param dockerFolder The folder containing the Dockerfile and
     *                     other files required by the Dockerfile.
     * @param repository   The repository name and optionally tag to assign
     *                     to the built image.
     * @param buildConfig  The build configuration describing which parameters
     *                     to include for the Docker build command.
     */
    void build(@NotNull final File dockerFolder, @NotNull final String repository,
               @NotNull final BuildConfig buildConfig) throws DockerException;

    /**
     * Saves a Docker image to a file.
     *
     * @param filename The name of the output file.
     * @param repository The name of the Docker image to save.
     */
    void save(@NotNull final String filename, @NotNull final String repository) throws DockerException;

    /**
     * Tags a Docker image.
     *
     * @param image The name of the image to tag.
     * @param repository The new repository to assign to the image.
     * @param tag The new tag to assign to the image.
     */
    void tag(@NotNull final String image, @NotNull final String repository, @NotNull final String tag) throws DockerException;

    /**
     * Pushes a Docker image to a registry.
     *
     * @param repository The image name which may consist of registry
     *                   address, namespace, repository and tag parts.
     *                   Only repository is required. If the registry
     *                   address part is not provided the image will
     *                   be pushed to Docker Hub.
     * @param authConfig The authorisation details for the Docker registry.
     */
    void push(@NotNull final String repository, @NotNull final AuthConfig authConfig) throws DockerException;

    /**
     * Pulls a Docker image from a registry.
     *
     * @param repository The image name which may consist of registry address, namespace, repository and tag parts. Only repository is required. If the registry
     *                   address part is not provided the image will be pulled from Docker Hub.
     * @param authConfig The authorisation details for the Docker registry.
     */
    void pull(@NotNull final String repository, @NotNull final AuthConfig authConfig) throws DockerException;
}
