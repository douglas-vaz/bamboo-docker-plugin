package com.atlassian.bamboo.plugins.docker;

import com.google.common.base.Splitter;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collections;
import java.util.Map;

public abstract class CustomBuildDataHelper
{
    private static final String DEPLOYED_SERVICE_URL = "deployedServiceUrl";
    private static final String RUN_CONTAINER_NAMES = "dockerContainerNames";

    public static void setDeployedServiceUrl(@NotNull final Map<String, String> customData, @NotNull String deployedServiceUrl)
    {
        customData.put(DEPLOYED_SERVICE_URL, deployedServiceUrl);
    }

    @Nullable
    public String getDeployedServiceURL(@NotNull final Map<String, String> customData)
    {
        return customData.get(DEPLOYED_SERVICE_URL);
    }

    public static void registerContainer(@NotNull final Map<String, String> customData, @NotNull String name)
    {
        final String containerNames = customData.get(RUN_CONTAINER_NAMES);
        customData.put(RUN_CONTAINER_NAMES, StringUtils.join(containerNames, ' ', name).trim());
    }

    @NotNull
    public static Iterable<String> getDetachedContainers(@NotNull final Map<String, String> customData)
    {
        final String containerNames = customData.get(RUN_CONTAINER_NAMES);

        if (StringUtils.isBlank(containerNames))
        {
            return Collections.EMPTY_SET;
        }

        return Splitter.on(' ').split(containerNames);
    }
}