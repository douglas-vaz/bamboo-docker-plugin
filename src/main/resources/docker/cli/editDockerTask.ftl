<div class="docker-plugin">
[@s.select labelKey='docker.command' name='commandOption' toggle='true' cssClass="long-field"
    list='commandOptions' listKey='first' listValue='second' /]
[@ui.bambooSection dependsOn='commandOption' showOn='build']
    [#include "dockerBuildForm.ftl"]
[/@ui.bambooSection]
[@ui.bambooSection dependsOn='commandOption' showOn='run']
    [#include "dockerRunForm.ftl"]
[/@ui.bambooSection]
[@ui.bambooSection dependsOn='commandOption' showOn='push']
    [#include "dockerPushForm.ftl"]
[/@ui.bambooSection]
[@ui.bambooSection dependsOn='commandOption' showOn='pull']
    [#include "dockerPullForm.ftl"]
[/@ui.bambooSection]
[@ui.bambooSection id='advancedOptionsSection' titleKey='repository.advanced.option' collapsible=true
isCollapsed=!(environmentVariables?has_content || workingSubDirectory?has_content)]
    [@s.textfield labelKey='builder.common.env' name='environmentVariables' cssClass="long-field" /]
    [@s.textfield labelKey='builder.common.sub' name='workingSubDirectory' cssClass="long-field" /]
[/@ui.bambooSection]
</div>