[@ww.radio labelKey='docker.registry' name='pullRegistryOption' toggle='true'
list='registryOptions' listKey='first' listValue='second' /]
[@s.textfield labelKey='docker.pull.repository' name='pullRepository' cssClass="long-field" required=true /]
[@ui.bambooSection dependsOn='pullRegistryOption' showOn='hub']
    <div class="description">[@s.text name='docker.pull.repository.hub.description' /]</div>
[/@ui.bambooSection]
[@ui.bambooSection dependsOn='pullRegistryOption' showOn='custom']
    <div class="description">[@s.text name='docker.pull.repository.custom.description' /]</div>
[/@ui.bambooSection]
<br/>
[@ui.bambooSection titleKey="docker.credentials"]
    <div class="description">[@s.text name='docker.credentials.description'/]</div>
    [@s.textfield labelKey='docker.username' name='pullUsername' cssClass="long-field" /]
    [#if pullPassword?has_content]
        [@s.checkbox labelKey='docker.password.change' toggle=true name='pullChangePassword' /]
        [@ui.bambooSection dependsOn='pullChangePassword']
            [@s.password labelKey='docker.password' name='pullPassword' cssClass="long-field" /]
        [/@ui.bambooSection]
    [#else]
        [@s.hidden name='pullChangePassword' value='true' /]
        [@s.password labelKey='docker.password' name='pullPassword' cssClass="long-field" /]
    [/#if]
    [@s.textfield labelKey='docker.email' name='pullEmail' cssClass="long-field" /]
[/@ui.bambooSection]